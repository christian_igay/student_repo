<?php
namespace App\View;

class TemplateView {

    public function showContent($data)
    {
        include('app/View/templates/header.php');
        include('app/View/pages/showUser.php');
        include('app/View/templates/footer.php');
    }
}