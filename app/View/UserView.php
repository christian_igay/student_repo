<?php
namespace App\View;

use App\Interfaces\ViewInterface;
use App\View\TemplateView;

class UserView extends TemplateView implements ViewInterface {
    

    public function display($data)
    {
        $this->showContent($data);       
    }
}