<?php
namespace App\Controllers;

use App\Interfaces\ViewInterface;
use App\Models\UserModel;
use App\Controllers\FileController;

class UserController {

    public function __construct(ViewInterface $viewInterface)
    {
        $this->userModel = new UserModel;
        $this->viewInterface = $viewInterface;
    }
    public function showUser()
    {
        $this->filecontroller = new FileController;
        $uploadResult = $this->filecontroller->upload('myFiles');
        $data = $_POST;
        $data = $_POST;
        $data['image_file'] = $uploadResult['success'][0]['web_path'] ?? 'storage/' . 'image_placeholder.png';
        $this->userModel->setUser($data);
        $user = $this->userModel->getUser();
        $this->viewInterface->display($user);
        
    }
}