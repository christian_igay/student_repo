<?php
namespace App\Models;

class UserModel {
    public $firstname;
    public $email;
    public $mobile;
    public $password;
    public $image_file;


    public function setUser($data)
    {
        $this->firstname = $data['firstname'] ?? '';
        $this->email = $data['email'] ?? '';
        $this->mobile = $data['mobile'] ?? '';
        $this->password = $data['password'] ?? '';
        $this->image_file = $data['image_file'] ?? '';
    }

    public function getUser()
    {
        return $this;
    }
}