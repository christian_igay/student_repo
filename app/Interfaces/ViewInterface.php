<?php
namespace App\Interfaces;

interface ViewInterface {

    public function display($data);
}