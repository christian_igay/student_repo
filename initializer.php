<?php
defined('UPLOAD_DIR') || define('UPLOAD_DIR', getcwd() . '/storage/');
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
include_once('vendor/autoload.php');